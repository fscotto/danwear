"""
WSGI config for Danward project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/howto/deployment/wsgi/
"""

import os
import sys
import site

site.addsitedir('/var/envs/django-danward/lib/python2.7/site-packages')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "Danward.settings")
sys.path.append('/var/apps/django-danward')

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
