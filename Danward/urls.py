from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/collections/', 'core.api.endpoints.collections_list'),
    url(r'^api/press/(\d+)/', 'core.api.endpoints.press_detail'),
    url(r'^api/press/', 'core.api.endpoints.press_list'),
    url(r'^api/stores/', 'core.api.endpoints.stores_list'),
)
