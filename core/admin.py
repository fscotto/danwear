from django.contrib import admin
from core.models import Collection, Continent, Press, Store, PressPage

# Register your models here.
admin.site.register(Continent)
admin.site.register(Collection)
admin.site.register(Press)
admin.site.register(Store)
admin.site.register(PressPage)
