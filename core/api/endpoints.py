from django.core import serializers
from django.views.decorators.http import require_http_methods
from django.http.response import HttpResponse
from django.shortcuts import get_object_or_404
import json

from iquii.http import HttpResponseDone
from iquii.utils import create_error
from iquii.decorators import required_params

from core.models import Collection, Press, Store, Continent, PressPage

EXCLUDED_FIELDS = ('password', 'user_permissions', 'groups', 'is_active')

@require_http_methods(["GET"])
@required_params(params=['type'])
def collections_list(request):
    collections = Collection.objects.filter(type=request.GET.get('type')).order_by('number')
    data = serializers.serialize(format='json', queryset=collections)
    return HttpResponseDone(content=data)


@require_http_methods(["GET"])
def press_list(request):
    press = Press.objects.all().order_by('order_number')
    data = serializers.serialize(format='json', queryset=press, extras=('num_pages', ))
    return HttpResponseDone(content=data)


@require_http_methods(["GET"])
def press_detail(request, pk):
    press = get_object_or_404(Press, pk=pk)
    press_pages = PressPage.objects.filter(press=press).order_by('order_number')
    data = serializers.serialize(format='json', queryset=press_pages)
    return HttpResponseDone(content=data)

@require_http_methods(['GET'])
def stores_list(request):
    store = Store.objects.all()
    continents = Continent.objects.all()
    continents_array = []
    for continent in continents:
        store = Store.objects.filter(continent=continent)
        store_array = []
        for s in store:
            element = {"continent": s.continent.continent,
                       "city": s.city,
                       "name": s.name,
                       "icon": 'http://danwardwear.com%s' % s.icon.url}  # TODO change link
            store_array.append(element)
        continents_array.append({"continent": continent.continent, "stores": store_array})
    response = continents_array
    return HttpResponse(json.dumps(response), content_type='application/json')

