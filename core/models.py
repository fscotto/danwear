from django.db import models
import uuid

def get_file_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return filename

collection_choices = ((1, 'Resortwear'),
                      (2, 'Footwear'))

class Collection(models.Model):
    name = models.TextField(max_length=250, blank=True)
    number = models.IntegerField(blank=True)
    type = models.IntegerField(choices=collection_choices)
    high_resolution = models.ImageField(upload_to=get_file_path)
    low_resolution = models.ImageField(upload_to=get_file_path)
    timestamp = models.DateTimeField(auto_now_add=True)
    store = models.TextField(max_length=250, blank=True)

    def __unicode__(self):
        return u'%s' % self.name


class Press(models.Model):
    name = models.TextField(max_length=250)
    high_resolution = models.ImageField(upload_to=get_file_path)
    low_resolution = models.ImageField(upload_to=get_file_path, blank=True)
    order_number = models.IntegerField(null=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    def num_pages(self):
        return PressPage.objects.filter(press=self).count()

    def __unicode__(self):
        return u'%s' % self.name


class PressPage(models.Model):
    press = models.ForeignKey(Press)
    name = models.TextField(max_length=250)
    high_resolution = models.ImageField(upload_to=get_file_path)
    low_resolution = models.ImageField(upload_to=get_file_path, blank=True)
    order_number = models.IntegerField(null=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return u'%s' % self.name

class Continent(models.Model):
    continent = models.TextField(max_length=50)

    def __unicode__(self):
        return u'%s' % self.continent

class Store(models.Model):
    continent = models.ForeignKey(Continent)
    nation = models.TextField(max_length=50)
    city = models.TextField(max_length=50)
    name = models.TextField(max_length=150)
    icon = models.ImageField(upload_to=get_file_path)

    def __unicode__(self):
        return u'%s | %s | %s' % (self.nation, self.city, self.name)

