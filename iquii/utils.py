__author__ = 'Fabio Ciotoli'
import json

def create_error(_dict):
    key = 'error_message'
    return json.dumps({key: get_error_from_dict(_dict)})

def get_error_from_dict(_dict):
    try:
        errors = []
        for key in _dict.keys():
            errors.append("%s: %s" % (key, _dict[key][0]))
        return errors
    except:
        if type(_dict) is str:
            return _dict
        else:
            return 'Unknown error'

#
#   Get the filters in the request.GET
#
def get_filters(request, extras={}, excludes=None):
    filters = {}
    for key, value in request.GET.iteritems():
        if key not in excludes:
            filters.update({key: value})
    filters.update(extras)
    return filters

