__author__ = 'Fabio Ciotoli'

from django.http.response import HttpResponse, HttpResponseBase

class HttpResponseDone(HttpResponse):
    def __init__(self, *args, **kwargs):
        kwargs['content_type'] = 'application/json'
        kwargs['status'] = 200
        super(HttpResponseDone, self).__init__(*args, **kwargs)

class HttpResponseCreated(HttpResponse):
    def __init__(self, *args, **kwargs):
        kwargs['content_type'] = 'application/json'
        kwargs['status'] = 201
        super(HttpResponseDone, self).__init__(*args, **kwargs)

class HttpResponseDeleted(HttpResponse):
    def __init__(self, *args, **kwargs):
        kwargs['content_type'] = 'application/json'
        kwargs['status'] = 204
        super(HttpResponseDeleted, self).__init__(*args, **kwargs)

class HttpResponseInvalid(HttpResponse):
    def __init__(self, *args, **kwargs):
        kwargs['content_type'] = 'application/json'
        kwargs['status'] = 400
        super(HttpResponseInvalid, self).__init__(*args, **kwargs)

class HttpResponseUnauthorized(HttpResponse):
    def __init__(self, *args, **kwargs):
        kwargs['content_type'] = 'application/json'
        kwargs['status'] = 401
        super(HttpResponseUnauthorized, self).__init__(*args, **kwargs)

class HttpResponseForbidden(HttpResponse):
    def __init__(self, *args, **kwargs):
        kwargs['content_type'] = 'application/json'
        kwargs['status'] = 403
        super(HttpResponseForbidden, self).__init__(*args, **kwargs)

class HttpResponseNotFound(HttpResponse):
    def __init__(self, *args, **kwargs):
        kwargs['content_type'] = 'application/json'
        kwargs['status'] = 405
        super(HttpResponseNotFound, self).__init__(*args, **kwargs)

class HttpResponseServerError(HttpResponse):
    def __init__(self, *args, **kwargs):
        kwargs['content_type'] = 'application/json'
        kwargs['status'] = 500
        super(HttpResponseServerError, self).__init__(*args, **kwargs)


