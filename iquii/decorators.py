__author__ = 'Fabio Ciotoli'
from http import HttpResponseInvalid
from utils import create_error

from functools import wraps

def required_params(method='GET', params=[]):
    def _required_params(view_func):
        def _decorator(request, *args, **kwargs):
            for param in params:
                if method == 'GET' and param not in request.GET:
                    return HttpResponseInvalid(create_error('Required param: %s' % param))
                elif method == 'POST' and param not in request.POST:
                    return HttpResponseInvalid(create_error('Required param: %s' % param))
                elif method == 'PUT' and param not in request.PUT:
                    return HttpResponseInvalid(create_error('Required param: %s' % param))
                elif method == 'DELETE' and param not in request.DELETE:
                    return HttpResponseInvalid(create_error('Required param: %s' % param))
            response = view_func(request, *args, **kwargs)
            return response
        return wraps(view_func)(_decorator)
    return _required_params
